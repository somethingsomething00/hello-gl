#include <stdio.h>
#include <stdbool.h>


#include <GL/gl.h>
#include <GLFW/glfw3.h>


bool Running = 1;

void CbResize(GLFWwindow *Window, int w, int h)
{
	// Adjust the viewable area to match the client area of the window
	// Comment it out and see what happens
	glViewport(0, 0, w, h);
}

void CbClose(GLFWwindow *Window)
{
	Running = false;
}

int main(int argc, char **argv)
{
	// Create the window
	glfwInit();
	GLFWmonitor *Monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode *Vidmode = glfwGetVideoMode(Monitor);
	GLFWwindow *Window = glfwCreateWindow(800, 600, "Hello GL", 0, 0);
	glfwMakeContextCurrent(Window);

	// GLFW will call this function every time the window is resized
	glfwSetFramebufferSizeCallback(Window, CbResize);

	// GLFW will call this function when the window "close" button is pressed
	glfwSetWindowCloseCallback(Window, CbClose);

	int interval = 1;
	// The system will wait interval * refresh_rate_seconds before displaying the frame
	// For a 60 hz display, this is 1/60 * 1
	glfwSwapInterval(1);


// GL info
//--------------------------------------------------------------------------------
	const char *version = glGetString(GL_VERSION);
	const char *vendor = glGetString(GL_VENDOR);
	const char *renderer = glGetString(GL_RENDERER);

	printf("=========================================================\n");
	printf("Version: %s\n", version);
	printf("Vendor: %s\n", vendor);
	printf("Renderer: %s\n", renderer);
	printf("=========================================================\n");



	/*****************************************************************************************
	* Brief intro to OpenGL
	* OpenGL is essentially a giant global state machine
	* Without reading the documentation, it's not clear which functions should go together
	* and how they affect each-other
	* www.docs.gl
	*****************************************************************************************/
	// The background color that will be set when glClear is called with GL_COLOR_BUFFER_BIT
	// RGBA
	glClearColor(0, 0, 0, 1);

	// Enables alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	while(Running)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		// Reset the global matrix for the current matrix mode
		glLoadIdentity();

// Hello Triangle
//--------------------------------------------------------------------------------		
		// By default, OpenGL's coordinate system goes from -1, 1 in all axis'
		// The origin (-1, -1) is the bottom left of the screen
		// Colors are interpolated per vertex
		// Attributes like color and texture coordinates must be specified before calling glVertex

		glColor3f(1, 1, 1);
		glBegin(GL_TRIANGLES);

		// Left
		glColor3f(1, 0, 0);
		glVertex2f(-0.5, -0.5);

		// Centre
		glColor3f(0, 1, 0);
		glVertex2f(0, 0.5);

		glColor3f(0, 0, 1);
		glVertex2f(0.5, -0.5);

		glEnd();

		// Configuring the coordinates that OpenGL uses is done through matrix operations
		// For instance, glOrtho sets up a logical 2D window (nice for 2D games and HUD elements)
		// Now the origin is at the top-left
		// This will draw a quad at the top of the triangle to demonstrate alpha blending
		float SCREEN_WIDTH = 480;
		float SCREEN_HEIGHT = 640;
		float QUAD_WIDTH = 50;
		float QUAD_HEIGHT = 50;
		
		float x0 = (SCREEN_WIDTH / 2) - (QUAD_WIDTH / 2);
		float x1 = x0 + QUAD_WIDTH;
		
		float y0 = (SCREEN_HEIGHT / 4) - (QUAD_HEIGHT / 2);
		float y1 = y0 + QUAD_HEIGHT;

		// Save the global matrix stack and load in our orthographic projection
		// All matrix operations will be multiplied by the current matrix stored by OpenGL, so it's important to call
		// glLoadIdentity() if you intend to use a new matrix

		// Translation, rotation, and scaling is not covered here for now

		glPushMatrix();
		glLoadIdentity();
		glOrtho(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, 1);

		glColor4f(0.2, 0.3, 0.4, 0.8);
		glBegin(GL_QUADS);

		glVertex2f(x0, y0);
		glVertex2f(x0, y1);
		glVertex2f(x1, y1);
		glVertex2f(x1, y0);
		
		glEnd();

		glPopMatrix();


// Handle input
//--------------------------------------------------------------------------------		
		if(glfwWindowShouldClose(Window))
		{
			Running =  false;
		}
		if(glfwGetKey(Window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			Running = false;
		}

		if(glfwGetKey(Window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			Running = false;
		}


// Swap buffers and display
//--------------------------------------------------------------------------------		
		glfwSwapBuffers(Window);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}
