#include <stdio.h>
#include <stdbool.h>


#include <GL/gl.h>
#include <GLFW/glfw3.h>


bool Running = true;

void CbResize(GLFWwindow *Window, int w, int h)
{
	glViewport(0, 0, w, h);
}

void CbClose(GLFWwindow *Window)
{
	Running = false;
}

int main(int argc, char **argv)
{
	glfwInit();
	GLFWmonitor *Monitor = glfwGetPrimaryMonitor();
	GLFWwindow *Window = glfwCreateWindow(800, 600, "Hello GL", 0, 0);
	glfwMakeContextCurrent(Window);
	glfwSetFramebufferSizeCallback(Window, CbResize);
	glfwSetWindowCloseCallback(Window, CbClose);
	glfwSwapInterval(1);

	glClearColor(0, 0, 0, 1);
	glLoadIdentity();


	while(Running)
	{
		if(glfwGetKey(Window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			Running = false;
		}

		glClear(GL_COLOR_BUFFER_BIT);
		
		glColor3f(1, 1, 1);
		glBegin(GL_TRIANGLES);

		glColor3f(1, 0, 0);
		glVertex2f(-0.5, -0.5);

		glColor3f(0, 1, 0);
		glVertex2f(0, 0.5);

		glColor3f(0, 0, 1);
		glVertex2f(0.5, -0.5);

		glEnd();

		glfwSwapBuffers(Window);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}
