.PHONY: all clean

CC = gcc
LIBS= -lGL -lglfw
COMPILE_COMMAND = $(CC) $(LIBS)

ex0 = hello-gl-0
ex1 = hello-gl-1

all: $(ex0) $(ex1)


$(ex1): $(ex1).c
	$(COMPILE_COMMAND) $< -o $@

$(ex0): $(ex0).c
	$(COMPILE_COMMAND) $< -o $@

clean:
	rm -f $(ex0)
	rm -f $(ex1)

